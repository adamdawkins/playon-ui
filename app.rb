require 'compass'
require 'sinatra'
require 'haml'

class PlayonUI < Sinatra::Base
  # Set Sinatra variables
  set :app_file, __FILE__
  set :root, File.dirname(__FILE__)
  set :views, 'views'
  set :public_folder, 'public'
  set :haml, {:format => :html5} # default Haml format is :xhtml

  configure do
    set :haml, {:format => :html5}
    set :scss, {:style => :compact, :debug_info => false}
    Compass.add_project_configuration(File.join(Sinatra::Application.root, 'config', 'compass.rb'))
  end

  before '/*' do
    @level = request.env['rack.request.query_hash']['level']
    @level ||= 'ballboy'
  end
  # Application routes
  get '/' do
    
    haml :index, :layout => :'layouts/application'
  end

  get '/dashboard' do
    haml :dashboard, :layout => :'layouts/application'
  end

  get '/search' do
    haml :search_results, :layout => :'layouts/application'
  end

  get '/team/matchday*' do
    haml :matchday, :layout => :'layouts/application'
  end

  get '/team/results' do
    haml :results, :layout => :'layouts/application'
  end

  get '/team/results/empty' do
    haml :results_no_team, :layout => :'layouts/application'
  end

  get '/team/live/matchday' do
    haml :matchday, :layout => :'layouts/application'
  end

  get '/team/live/trial*' do
    haml :trial, :layout => :'layouts/application'
  end

  get '/team/live/empty' do
    haml :matchday_no_team, :layout => :'layouts/application'
  end

  get '/games' do
    @game = request.query_string
    haml :games, :layout => :'layouts/application'
  end

  get '/profile' do
    haml :profile, :layout => :'layouts/application'
  end

  get '/profile/*' do
    haml :profile_ambassador, :layout => :'layouts/application'
  end

  get '/club/lobby' do
    @clubs = %w[arsenal aston-villa cardiff-city chelsea crystal-palace everton fulham hull-city liverpool manchester-city manchester-united newcastle-united norwich-city southampton stoke-city sunderland swansea-city tottenham-hotspur west-bromwich-albion west-ham-united]
    haml :club_lobby, :layout => :'layouts/application'
  end

  get '/club/:slug' do
  @slug = params[:slug]
  @club_name = club_name(@slug)
  @abbreviation = @club_name.slice(0,3).upcase 
    haml :my_club, :layout => :'layouts/application'
  end  

  get '/how-to-play/introduction' do
    haml :how_to_play_introduction, :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/pick-team' do
    haml :how_to_play_pick_team, :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/enter-games' do
    haml :how_to_play_enter_games, :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/live-matchday' do
    haml :how_to_play_matchday, :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/results' do
    haml :how_to_play_results, :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/rewards' do
    haml :how_to_play_rewards, :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/scoring-matrix' do
    haml :'how_to_play_scoring-matrix', :layout => :'layouts/how_to_play'
  end

  get '/how-to-play/contact-us' do
    haml :how_to_play_contact_us, :layout => :'layouts/how_to_play'
  end

  get '/account/registration/step-1' do
    haml :account_registration_step1, :layout => :'layouts/account'
  end

  get '/account/registration/step-2' do
    haml :account_registration_step2, :layout => :'layouts/account'
  end

  get '/account/registration/step-3' do
    haml :account_registration_step3, :layout => :'layouts/account'
  end

  get '/account/registration/step-4' do
    haml :account_registration_step4, :layout => :'layouts/account'
  end

  get '/account/password-assistance' do
    haml :account_password_assistance, :layout => :'layouts/account'
  end

  get '/account/details' do
    haml :account_details, :layout => :'layouts/account'
  end

  get '/account/deposit/limit' do
    haml :account_limit_deposit, :layout => :'layouts/account'
  end

  get '/account/deposit' do
    haml :account_deposit, :layout => :'layouts/account'
  end

  get '/account/deposit/success' do
    haml :account_deposit_success, :layout => :'layouts/account'
  end

  get '/account/deposit/error' do
    haml :account_deposit_error, :layout => :'layouts/account'
  end

  get '/account/notifications' do
    haml :account_notifications, :layout => :'layouts/account'
  end

  get '/account/rewards' do
    haml :account_rewards, :layout => :'layouts/account'
  end

  get '/account/club-points' do
    haml :account_club_points, :layout => :'layouts/account'
  end

  get '/account/club-points/join' do
    haml :account_club_join, :layout => :'layouts/account'
  end

  get '/account/suspend' do
    haml :account_suspend, :layout => :'layouts/account'
  end

  get '/account/close' do
    haml :account_close, :layout => :'layouts/account'
  end

  get '/account/transaction-history' do
    haml :account_transaction_history, :layout => :'layouts/account'
  end

  get '/account/verify' do
    haml :account_verify, :layout => :'layouts/account'
  end

  get '/account/withdraw' do
    haml :account_withdraw, :layout => :'layouts/account'
  end

  get '/account/withdraw/success*' do
    haml :account_withdraw_success, :layout => :'layouts/account'
  end

  get '/terms-and-conditions' do
    haml :terms_and_conditions, :layout => :'layouts/application'
  end

  get '/responsible-gaming' do
    haml :responsible_gaming, :layout => :'layouts/application'
  end

  get '/game-rules' do
    haml :game_rules, :layout => :'layouts/application'
  end

  get '/about' do
    haml :about, :layout => :'layouts/application'
  end

  get '/payout-structure' do
    haml :payout_structure, :layout => :'layouts/application'
  end

  get '/privacy-policy' do
    haml :privacy_policy, :layout => :'layouts/application'
  end

  get '/rewards' do
    haml :rewards, :layout => :'layouts/application'
  end

  get '/rewards-popup' do
    haml :rewards_popup, :layout => :'layouts/application'
  end

  get '/scoring-matrix' do
    haml :scoring_matrix, :layout => :'layouts/application'
  end

  get '/team/pick*' do
    haml :pick_team, :layout => :'layouts/application'
  end

  get '/games/one-on-one/live*' do
    haml :'one-on-one_live', :layout => :'layouts/application'
  end

  get '/games/one-on-one*' do
    haml :'one-on-one', :layout => :'layouts/application'
  end

  get '/games/multiplayer/live*' do
    haml :'multiplayer_live', :layout => :'layouts/application'
  end

  get '/games/multiplayer*' do
    haml :'multiplayer', :layout => :'layouts/application'
  end


  get '/404' do
    haml :'404', :layout => :'layouts/error'
  end

  get '/500' do
    haml :'500', :layout => :'layouts/error'
  end

  get '/leaderboard' do
    haml :leaderboard, :layout => :'layouts/application'
  end  

  get '/reset-password*' do
    haml :reset_password, :layout => :'layouts/application'
  end  


  helpers do
    def partial(page, options={})
      haml page, options.merge!(:layout => false)
    end

    def club_name(slug)
      slug.gsub(/-/, " ").gsub(/^[a-z]|\s+[a-z]/) { |a| a.upcase}
    end

    def club_abbr(club_name)
      club_name.slice(0,3).upcase 
    end
  end
 
  run! if app_file == $0
end
