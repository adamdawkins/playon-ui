Feature: View details of live one-on-one game
  In order to see how my team is doing
  As a PlayON player
  I want to compare my team to my opponent

  Scenario: View one-on-one game
  Given I am on the live matchday page
  When I click a 'view' button in the one-on-one games table
  Then I should see the Compare Pitch modal

  Scenario: Close compare pitch modal
  Given I have selected a one-on-one game
  And I am on the Compare Pitch Modal
  When I close the modal
  Then I should see the live matchday page

  Scenario: Switch to Compare List View
  Given I am on the Compare Pitch Modal
  When I click the 'View as List' button
  Then I should see the Compare as list modal

  Scenario: Close compare list modal
  Given I have selected a one-on-one game
  And I am on the Compare list Modal
  When I close the modal
  Then I should see the live matchday page

  Scenario: Switch to Compare Pitch view
  Given I am on the Compare list Modal
  When I click 'View Pitch
  Then I should see the Compare Pitch modal
