Feature: View details of live multiplayer game
  In order to see how my team is doing
  As a PlayON player
  I want to compare my team to other teams

  Scenario: View multiplayer game leaderboard
  Given I am on the live matchday page
  When I click on the 'view' button in the multiplayer games table
  Then I should see the multiplayer leaderboard modal

  Scenario: Compare to another team
  Given I am on the leaderboard modal
  When I click a 'Compare' button
  Then I should see the Compare Pitch modal

  Scenario: Close compare pitch modal
  Given I have selected a multiplayer game
  And I am on the Compare Pitch Modal
  When I close the modal
  Then I should see the leaderboard modal

  Scenario: Switch to Compare List View
  Given I am on the Compare Pitch Modal
  When I click the 'View as List' button
  Then I should see the Compare as list modal

  Scenario: Close compare list modal
  Given I have selected a multiplayer game
  And I am on the Compare list Modal
  When I close the modal
  Then I should see the leaderboard modal

  Scenario: Switch to Compare Pitch view
  Given I am on the Compare list Modal
  When I click 'View Pitch
  Then I should see the Compare Pitch modal
