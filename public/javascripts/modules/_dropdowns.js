$(document).ready( function () {
  $('.m-dropdown--link').on('click', function() {
    $(this).parent('.m-dropdown').toggleClass('open');

    if($(this).hasClass('login')) {
      $('#login-form').find('.email').focus();  
    }

    $('.dk_open').removeClass('dk_open');
  });

  $('.m-dropdown').bind( 'clickoutside', function(event){
    $(this).removeClass('open');
  });

});
