// Generated by CoffeeScript 1.4.0
(function() {

  $(document).ready(function() {
    var table;
    table = $('.data-table').dataTable({
      'bLengthChange': false,
      "aoColumns": [
        null, null, null, null, null, null, {
          "bVisible": false
        }
      ]
    });
    $('.filter', '#join-multiplayer').change(function() {
      console.log('change: ' + this.value);
      return table.fnFilter(this.value, 6);
    });
    return $('#clear').click(function() {
      console.log('click');
      return table.fnFilter('');
    });
  });

}).call(this);
