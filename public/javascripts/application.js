$(document).ready( function () {

  var  $leaderboardModal        = $('#leaderboard_modal')
    ,  $pitchCompareModal       = $('#pitch_compare_modal')
    ,  $compareListModal        = $('#compare_list_modal')
    ,  $viewOneOnOneTicketModal = $('#view_one-on-one_ticket_modal')
    ,  $oneOnOneConfirmModal    = $('#one-on-one_confirm_modal')
    ,  $viewMultiplayerModal    = $('#view_multiplayer_game_modal')
    ,  $multiplayerConfirmModal = $('#multiplayer_confirm_modal')
    ,  $signUpModal             = $('#sign_up_modal')
    ,  $forgottenPasswordModal  = $('#forgotten_password_modal')
    ,  $logInModal              = $('#log_in_modal')
    ,  $pageContainer           = $('#page_container');

 
  // only display background if page_container is over a certain height
  if($pageContainer.height() < 1350) {
    $pageContainer.addClass('no-background-image');
  }
     
  $(document).tooltip({selector: '[data-toggle^=tooltip]'});

  $('#sign_up_user').submit(function(e){ 
    $signUpModal.modal('hide');
    $('#demo_modal').modal('show');
    e.preventDefault();
  });

  $('#navigation_top-bar').find('.dropkick').dropkick({
    theme: 'options'  
  });

  $('#team_selection_header-bar').find('.dropkick').dropkick({
    theme: 'team'  
  });

  $('#navigation_options-bar').find('.dropkick').dropkick({
    theme: 'options'  
  });

  $signUpModal.on('click', '#or-login', function () {
    $signUpModal.modal('hide');
    $logInModal.modal('show');
  });

  $logInModal.on('click', '#or-signup', function () {
    $logInModal.modal('hide');
    $signUpModal.modal('show');
  });

  $logInModal.on('click', '#forgotten-password', function () {
    $logInModal.modal('hide');
    $forgottenPasswordModal.modal('show');
  });

  $forgottenPasswordModal.on('click', '#send-password', function () {
    $forgottenPasswordModal.modal('hide');
    $('#sent_password_modal').modal('show');
  });

$viewMultiplayerModal.on('click', '#join_game_button', function () {
  $multiplayerConfirmModal.data('from-ticket-modal', true);
  $viewMultiplayerModal.modal('hide');
  $multiplayerConfirmModal.modal('show');
});

$multiplayerConfirmModal.on('hide', function () {
  if($multiplayerConfirmModal.data('from-ticket-modal')) {
    $viewMultiplayerModal.modal('show');
  }
  $multiplayerConfirmModal.data('from-ticket-modal', false);
});

$viewOneOnOneTicketModal.on('click', '#create_challenge_button', function () {
  $oneOnOneConfirmModal.data('from-ticket-modal', true);
  $viewOneOnOneTicketModal.modal('hide');
  $oneOnOneConfirmModal.modal('show');
});

  $oneOnOneConfirmModal.on('hide', function () {
    if ($oneOnOneConfirmModal.data('from-ticket-modal')) {
      $viewOneOnOneTicketModal.modal('show');
    }
    $oneOnOneConfirmModal.data('from-ticket-modal', false);
  });

  $leaderboardModal.on('click', '.m-button_table', function () { 
      $leaderboardModal.modal('hide');
      $pitchCompareModal.modal('show');
  });

  $('#view_list_button').on('click', function () {
    $pitchCompareModal.modal('hide');
    $compareListModal.modal('show');
  });

  $('#view_pitch_button').on('click', function () {
    $compareListModal.modal('hide');
    $pitchCompareModal.modal('show');
  });

  $('.back_to_details_button').on('click', function () {
    $(this).parents('.modal').modal('hide');
    $leaderboardModal.modal('show');
  });


$('.group', '#account_navigation').on('click', '.m-account-navigation-item--link', function (e) {
  var $this = $(this);
  $this.toggleClass('open');
  $this.next('.m-account-navigation-item--list').slideToggle();

  e.preventDefault();
  
});

  if($leaderboardModal.length > 0) {
   $leaderboardModal.dropkickOnShow({
      theme: 'filters' 
   });
  }

  if($( '#view_multiplayer_game_modal' ).length > 0) {
   $('#view_multiplayer_game_modal').dropkickOnShow({
      context: '#whos-in-dropkick-filters',
      theme: 'filters'
   });
  }

  if($( '#player_compare_tab_link' ).length > 0) {
   $('#player_compare_tab_link').dropkickOnShow({
      context: '#modal-dropkick-filters',
      theme: 'small-filters'
   });

   $('#player_compare_tab_link').dropkickOnShow({
      context: '#compare_table',
      theme: 'extra-small-filters'
   });
  }

  $('#dropkick-filters').find('.dropkick').dropkick({
    theme: 'filters'
  });

  $('#matchday_pitch .m-pitch-player--info').on('click', function () {
    var topPosition = $(this).offset().top - $('#matchday_pitch').offset().top - 2,
    leftPosition = $(this).offset().left - $('#matchday_pitch').offset().left - 70;

    $('#matchday-player-info').css(
      {
        'top': topPosition + 'px',
        'left': leftPosition + 'px'
      }
     ); 

    $('#matchday-player-info').show(); 
      return false;
    });

    $('.m-matchday-player-info').find('.icon-close').click(function () {
      $('.m-matchday-player-info').hide();
      return false;
    });

  if($('.m-pitch_compare').length > 0) {
    $('.m-pitch_compare .m-pitch-player--info').positionMoreInfo({
      pitch: '.m-pitch_compare' 
    });
  }

  if($('.m-pitch_profile').length > 0) {
    $('.m-pitch_profile .m-pitch-player--info').positionMoreInfo({
      pitch: '.m-pitch_profile' 
    });
  }

  $('#account-section').find('.dropkick').dropkick({
    theme: 'account'
  });

  // hides other dropdowns when clicking on dropkick dropdowns
  $('.dk_container').on('click', function () {
    $('.dk_open').not($(this)).removeClass('dk_open'); 
    $('.m-dropdown').removeClass('open');
  });


  $('#ticket_carousel').carousel();

  var sliderOptions = { value: 4, min: 0, max: 9, step: 1 };
  var ambassadorSliderOptions = { value: 1, min: 0, max: 1, step: 1 };

  $('#ticket_carousel').find('.m-ticket_ambassador .slider').slider(ambassadorSliderOptions);

  $('#create_open_challenge_modal').find('.slider').slider(sliderOptions);
  $viewOneOnOneTicketModal.find('.slider').slider(sliderOptions);
  $('#create_multiplayer_game_modal').find('.slider').slider(sliderOptions);
  $('#one-on-one_invite_modal').find('.slider').slider(sliderOptions);

// account widthrdawal page
  $('#bank_details_division').hide();
  $('input[name="withdrawal_method"]').on('click', function () {
    if($(this).attr('id') === 'withdraw_to_card_radio') {
      $('#card_details_division').show();
      $('#bank_details_division').hide();
    } else {
      $('#card_details_division').hide();
      $('#bank_details_division').show();
    }
  });

$('#invite-contacts-button').on( 'click', function (e) {
  var $inviteContactsSectionInner = $('#invite-contacts-section--inner');

  $inviteContactsSectionInner.slideToggle();

  if(!$inviteContactsSectionInner.data('loaded-filters')) {
    $('#invite-dropkick-filters').find('.dropkick').dropkick({
      theme: 'filters'
    });
    $inviteContactsSectionInner.data('loaded-filters', true);
  }
  e.preventDefault(); 
});

  $('.icon-chat', document.body).click(function (e) {
    var offset = $(this).offset(),
      $chat = $('#chat'),
      $conversation = $chat.find('.m-chat-conversation');

    e.stopPropagation();

    $chat.offset({ top: offset.top, left: offset.left + 20 });
    $('#chat').css('visibility', 'visible');

    $conversation[0].scrollTop = $conversation[0].scrollHeight; // you want to run this line whenever a reply is added
  });

  $('body').click(function (e) {
    $('#chat').css('visibility', 'hidden');
    $('#ticket_carousel').carousel('cycle');
  });

  $(document).keyup(function (e) {
    if(e.keyCode === 27) {
      $('#chat').css('visibility', 'hidden');
      $('#ticket_carousel').carousel('cycle');
    }
  });

  $('.icon-chat', '#ticket_carousel').click(function (e) {
    $('#ticket_carousel').carousel('pause');
  });

  $('#chat').click(function (e) {
    e.stopPropagation();
  });

  $('.icon-close', '#chat').click(function (e) {
    $('#chat').css('visibility', 'hidden');
    e.preventDefault();
  });

  $('#add_my_club_modal').on('click', '.m-button', function (e){
    $('.m-modal--body', '#add_my_club_modal').html('<p class="m-modal--encouragement">Your club is being added by the PlayON team - we will be in touch shortly.</p>');
    e.preventDefault();
  });
  
  if($("#countdown").length > 0) {
    var now = new Date()
    ,   timeText = now.getFullYear() + "/" + (now.getMonth() + 1) + "/" + now.getDate() + " " + (now.getHours() + 1) + ":" + now.getMinutes() + ":" + now.getSeconds();
    $("#countdown").jCountdown({
      timeText: timeText,
      timeZone:0,
      style:"slide",
      color:"black",
      width: 125, 
      textGroupSpace: 55,
      textSpace:0,
      reflection:false,
      reflectionOpacity:10,
      reflectionBlur:0,
      dayTextNumber:2,
      displayDay:false,
      displayHour:true,
      displayMinute:true,
      displaySecond: true,
      displayLabel: false,
      onFinish:function(){
        $.ajax({url: $('#js-matchday-countdown').data('event-url')});
      }
  });
  }
  
  $('#join_club').click(function(e) {
    var $this = $(this);
    $this.text('You have joined');
    e.preventDefault();
  });

$('#live_matchday_countdown').find('.icon-close').click( function (e) {
  $('#live_matchday_countdown').fadeOut();
  e.preventDefault();
});

});
