(function ($) {
  $.fn.dropkickOnShow = function (options) {
    var s = $.extend({
      element: null,
      dropdownSelector: '.dropkick',
      theme: '',
      context: this,
      intervalDuration: 100
    }, options),
      dropdownElements = $(s.context).find(s.dropdownSelector),
      checkIntervalID,
      check,
      checkDimensions;


    check = function () {
      if (!dropdownElements.hasClass('dropkick-done')) {
        checkIntervalID = setInterval(checkDimensions, s.intervalDuration);
      }

    };

    checkDimensions = function () {
      if (dropdownElements.height() > 0 && dropdownElements.width() > 0) {
        clearInterval(checkIntervalID);
        dropdownElements.dropkick({
          theme: s.theme
        });
        dropdownElements.addClass('dropkick-done');
      }
    };

    return $(this).on('show', function () {
      check();
    });

  };
}(jQuery));
