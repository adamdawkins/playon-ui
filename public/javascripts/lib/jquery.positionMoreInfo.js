
(function ($) {
  $.fn.positionMoreInfo = function (options) {
    var s = $.extend({
      pitch: null ,
      playerInfo: '.m-matchday-player-info'

    }, options);
        
    return $(this).on('click', function(e) {
      
      var $this = $(this),
          $pitch = $this.closest(s.pitch),
          $playerInfo = $pitch.find(s.playerInfo),
          $row = $this.closest('.m-pitch-row'),
          $player = $this.closest('.m-pitch-player'),
          iconOffset = $this.offset(),
          pitchOffset = $pitch.offset(),
          iconTop = parseInt(iconOffset.top - pitchOffset.top, 10),
          iconLeft = parseInt(iconOffset.left - pitchOffset.left, 10),
          position = {},
          playerInfoTop;
      
      // set vertical position of player score summary
      if($row.hasClass('m-pitch-row_midfielder') || $row.hasClass('m-pitch-row_forward')) {
        position.top = iconTop - parseInt($playerInfo.css('height'), 10) + 5 + 'px';
      } else {
        position.top = iconTop - 2 + 'px';
      }

      // set horizontal position of player score summary
      if($player.hasClass('l-pitch-position-3') || $player.hasClass('l-pitch-position-4')) {
        position.left = iconLeft - 142 + 'px'; 
      } else {
        position.left = iconLeft - 70 + 'px';
      }

      $playerInfo.css(position);
      $playerInfo.show(); 

      e.preventDefault();
    });

  };
}(jQuery));

