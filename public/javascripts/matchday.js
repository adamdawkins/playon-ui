$(document).ready(function () {
  var $teamComparisonTable = $('#team_comparison_table'),
      $liveFixturesTable = $('#live_fixtures'),
      $liveFixturesTableFixtures = $liveFixturesTable.find('.m-table_live-fixtures--fixture'),
      $liveFixturesTableDetails = $liveFixturesTable.find('.m-fixture-details');
  
   
 $liveFixturesTableFixtures.click(function () {
   var $this = $(this);

   $liveFixturesTableDetails.hide();

    if($this.hasClass('open')) {
      $this.removeClass('open');
    } else {
      $this.next('.m-fixture-details').show();

      $liveFixturesTableFixtures.removeClass('open');
      $this.addClass('open');
    }
  }); 

  $teamComparisonTable.on('click', 'a', function (e) {
    console.log('click'); 
    var $this = $(this),
        $teamTable = $this.closest('table'),
        $playerRow = $this.closest('tr'),
        $teamRows = $teamTable.find('> tr'),
        $playerSummaryContainers = $teamTable.find('.m-player-summary--container');

    console.log($playerRow); 
    $playerSummaryContainers.hide();

    if($playerRow.hasClass('open')) {
      $playerRow.removeClass('open');
      console.log('$playerRow hasClass open'); 
    } else {
      console.log('$playerRow does not have class open'); 
      $playerRow.next('.m-player-summary--container').show();
      $teamRows.removeClass('open');
      $playerRow.addClass('open');
    }
    
    e.preventDefault(); 

  });

});
